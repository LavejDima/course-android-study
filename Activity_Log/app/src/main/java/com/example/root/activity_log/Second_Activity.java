package com.example.root.activity_log;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;


public class Second_Activity extends Activity {

    public static final String SEC_ACT_LOG_TAG = "Second_Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_);
        findViewById(R.id.toFirstActBtn).setOnClickListener(toFirstActBtnOCklListener);
        setLogMessage("create method");
    }

    @Override
    protected void onResume() {
        super.onResume();
        setLogMessage("resume method");
    }

    @Override
    protected void onPause() {
        super.onPause();
        setLogMessage("pause method");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        setLogMessage("destroy method");
    }

    @Override
    protected void onStop() {
        super.onStop();
        setLogMessage("stop method");
    }

    View.OnClickListener toFirstActBtnOCklListener = new View.OnClickListener() {


        @Override
        public void onClick(View view) {

            startActivity(new Intent(getBaseContext(), First_Activity.class));

        }
    };

    public void setLogMessage(String message) {
        Log.d(SEC_ACT_LOG_TAG, "This is " + SEC_ACT_LOG_TAG + " " + message);
    }
}
