package com.example.root.activity_log;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class First_Activity extends Activity {

    public static final String FIR_ACT_LOG_TAG = "First_Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        findViewById(R.id.toSecondActBtn).setOnClickListener(toSecondActBtnOCklListener);
        setLogMessage("create method");
    }

    @Override
    protected void onResume() {
        super.onResume();
        setLogMessage("resume method");
    }

    @Override
    protected void onPause() {
        super.onPause();
        setLogMessage("pause method");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        setLogMessage("destroy method");
    }

    @Override
    protected void onStop() {
        super.onStop();
        setLogMessage("stop method");
    }

    View.OnClickListener toSecondActBtnOCklListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            startActivity(new Intent(getBaseContext(), Second_Activity.class));
        }
    };

    public void setLogMessage(String message) {
        Log.d(FIR_ACT_LOG_TAG, "This is " + FIR_ACT_LOG_TAG + " " + message);
    }
}
