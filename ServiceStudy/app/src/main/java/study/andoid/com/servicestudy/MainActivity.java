package study.andoid.com.servicestudy;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements Constants {

    private Button startService;
    private Button stopService;
    private Button sendMess;
    private Intent intent;
    private ServiceConnection sConn;
    private MyService.MyBinder mMyBinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intent = new Intent(this, MyService.class);
        sConn = getSConn();
        initButtons();
        printToLog("Activity: onCreate");
    }

    private void initButtons() {
        startService = (Button) findViewById(R.id.startService);
        startService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startService(intent);
                bindService(intent, sConn, BIND_ADJUST_WITH_ACTIVITY);
            }
        });
        sendMess = (Button) findViewById(R.id.sendMess);
        sendMess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MyService.isAlive) {
                    EditText text = (EditText) findViewById(R.id.editText);
                    EditText minutesOfRepeat = (EditText) findViewById(R.id.minutesOfRepeat);
                    //get message from edit text
                    String stringFromEditText = text.getText().toString();
                    //get time from edit text
                    int minutesFromEditText = Integer.parseInt(minutesOfRepeat.getText().toString());
                    printToLog("Activity: text in edit text - " + stringFromEditText + " minutes is " + minutesFromEditText);
                    getMyService().createMessage(stringFromEditText, minutesFromEditText, getApplicationContext());
                    showToast("message was sent");
                } else {
                    showToast("start the service please");
                }
            }
        });
        stopService = (Button) findViewById(R.id.stopService);
        stopService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(intent);
            }
        });
    }

    ServiceConnection getSConn() {
        printToLog("Activity: get service connection");
        return new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mMyBinder = ((MyService.MyBinder) service);
                printToLog("MainActivity onServiceConnected");
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                printToLog("MainActivity onServiceDisconnected");
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        printToLog("Activity: onDestroy");
    }

    public MyService getMyService() {
        return mMyBinder.getService();
    }

    private void printToLog(String text) {
        Log.d(LOG_TAG, text);
    }

    private void showToast(String text) {
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
    }
}
