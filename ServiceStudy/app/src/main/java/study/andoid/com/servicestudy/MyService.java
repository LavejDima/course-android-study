package study.andoid.com.servicestudy;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class MyService extends Service implements Constants {

    static boolean isAlive;

    @Override
    public IBinder onBind(Intent intent) {
        printToLog("MyService onBind; ");
        return new MyBinder();
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        printToLog("MyService onRebind");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        printToLog("MyService onUnbind");
        return super.onUnbind(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startServiceForeground();
        isAlive = true;
        printToLog("MyService onCreate()");
        showToast("MyService started");
    }

    private void startServiceForeground() {
        Context context = this.getApplicationContext();
        Intent targetIntent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, targetIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Builder builder = new Notification.Builder(this)
                .setContentIntent(pendingIntent)
                .setContentTitle("MyService started")
                .setSmallIcon(android.R.drawable.sym_action_email);
        Notification notification = builder.build();
        startForeground(777, notification);
        printToLog("MyService: started foreground");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cancelRepeating(getApplicationContext());
        isAlive = false;
        printToLog("MyService onDestroy()");
        showToast("MyService stopped");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int minutes = 1;
        printToLog("MyService onStartCommand method");
        createMessage(minutes + " min. gone, let`s drink tea now", minutes, getApplicationContext());
        return START_STICKY;
    }

    /**
     * send message to MessageReceiver
     *
     * @param context of application
     * @param message to send
     */
    private void sendMessageToMessageReceiver(Context context, String message, int minutes) {
        Bundle extras = new Bundle();
        extras.putString(INTENT_MESSAGE_TAG, message);
        extras.putInt(INTENT_MINUTES_TAG, minutes);
        Intent intent = new Intent(context, MessageReceiver.class);
        intent.putExtras(extras);
        sendBroadcast(intent);
        printToLog("MyService sendMessageToMessageReceiver: minutes - " + minutes + "; message - " + message + ".");
    }

    /**
     * cancel the repeat of alarm
     *
     * @param context of application
     */
    private void cancelRepeating(Context context) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        am.cancel(pi);
        printToLog("MyService: cancelRepeating");
    }

    /**
     * stops old repeating cycle, and send message to MessageReceiver
     *
     * @param message
     * @param context
     */
    public void createMessage(String message, int minutes, Context context) {
        cancelRepeating(context);
        sendMessageToMessageReceiver(context, message, minutes);
    }

    private void printToLog(String text) {
        Log.d(LOG_TAG, text);
    }

    private void showToast(String text) {
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
    }

    class MyBinder extends Binder {
        MyService getService() {
            return MyService.this;
        }
    }

}
