package study.andoid.com.servicestudy;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;


public class AlarmReceiver extends BroadcastReceiver implements Constants {

    @Override
    public void onReceive(Context context, Intent intent) {
        String message = MessageReceiver.getMessage();
        sendNotification(message, context);
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        Log.d(LOG_TAG, "AlarmReceiver send notification, the message is - " + message);
    }

    /**
     * send notifications to status bar
     *
     * @param text    of notification
     * @param context of application
     */
    private void sendNotification(String text, Context context) {
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent targetIntent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, targetIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Builder mNotificationBuilder = new Notification.Builder(context)
                .setContentText(text)
                .setContentTitle(text != null ? text.substring(0, text.length() / 3) + "..." : "text is null")
                .setSmallIcon(android.R.drawable.sym_action_email)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_ALL);
        Notification notification = mNotificationBuilder.build();
        mNotificationManager.notify(1, notification);
    }
}
