package study.andoid.com.servicestudy;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MessageReceiver extends BroadcastReceiver implements Constants {

    //message from MyService
    private static String message;

    public MessageReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        //get intent extra messages
        String text = intent.getExtras().getString(INTENT_MESSAGE_TAG);
        int minutes = intent.getExtras().getInt(INTENT_MINUTES_TAG);
        //set the message
        setMessage(minutes + " min. gone, let`s " + text + " now!");
        //start new repeating cycle after getting message from MyService
        startMessaging(context, minutes);
        printToLog("MessageReceiver onReceive message- " + text + "; minutes - " + minutes);
    }

    /**
     * start alarming to Service broadcast receiver
     *
     * @param context
     */
    private void startMessaging(Context context, int minutes) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        am.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000 * minutes * 60, 1000 * minutes * 60, pi);
        printToLog("MessageReceiver startMessaging repeat, message is " + getMessage());
    }

    public static String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private void printToLog(String text) {
        Log.d(LOG_TAG, text);
    }

}
