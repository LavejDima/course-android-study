package study.andoid.com.servicestudy;

public interface Constants {
    //logging tag
    String LOG_TAG = "ServiceStudy";
    //intent extras tag
    String INTENT_MESSAGE_TAG = "message";
    String INTENT_MINUTES_TAG = "minutes";
}
