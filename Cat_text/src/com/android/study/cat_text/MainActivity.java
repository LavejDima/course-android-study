package com.android.study.cat_text;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends Activity {

	private String charset ="CP1251";
	private String nameOfFile = "cat_text.txt";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		displayText(readText(nameOfFile));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * read text from assert
	 * 
	 * @param fileName
	 * @return
	 */
	private String readText(String fileName) {
		String result = "";
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(getAssets().open(fileName), charset));
			String string = "";
			while ((string = br.readLine()) != null) {
				result += string + "\n";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * set text to text view
	 * 
	 * @param text
	 */
	private void displayText(String text) {
		((TextView) findViewById(R.id.textView)).setText(text);
	}
}
