package study.andoid.com.intentstudy;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener, Constants {

    private AlarmManagerBroadcastReceiver alarm;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
    }

    /**
     * set no repeating timer
     *
     * @param targetMillis
     */
    public void setOneTimeTimer(long targetMillis) {
        Context context = this.getActivity().getApplicationContext();
        if (alarm != null) {
            alarm.setOneTimeTimer(context, targetMillis);
        } else {
            Toast.makeText(context, "Alarm is null", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Log.d(LOG_TAG, "hours of day = " + hourOfDay + "; minutes = " + minute);
        alarm = new AlarmManagerBroadcastReceiver();
        setOneTimeTimer(setCalendarTime(hourOfDay, minute).getTimeInMillis());
    }

    /**
     * set calendar time
     *
     * @param hourOfDay
     * @param minute
     * @return calendar with target time
     */
    public Calendar setCalendarTime(int hourOfDay, int minute) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
        c.set(Calendar.MINUTE, minute);
        return c;
    }

}
