package study.andoid.com.intentstudy;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AlarmManagerBroadcastReceiver extends BroadcastReceiver implements Constants {

    @Override
    public void onReceive(Context context, Intent intent) {
        //get current time
        String msgStr = new SimpleDateFormat("k:mm:ss").format(new Date());
        //play the alarm signal
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        Ringtone ringtone = RingtoneManager.getRingtone(context.getApplicationContext(), notification);
        ringtone.play();
        //make toast
        Toast.makeText(context, msgStr, Toast.LENGTH_LONG).show();
    }

    /**
     * set no repeating timer
     *
     * @param context
     * @param targetMillis
     */
    public void setOneTimeTimer(Context context, long targetMillis) {
        //get alarm manager
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        //set PendingIntent to alarm manager
        am.set(AlarmManager.RTC_WAKEUP, targetMillis, pi);
        Log.d(LOG_TAG, "set one time timer, millis = " + targetMillis);
    }
}
