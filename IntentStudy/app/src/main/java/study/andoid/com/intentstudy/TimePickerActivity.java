package study.andoid.com.intentstudy;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class TimePickerActivity extends Activity implements View.OnClickListener, Constants {

    Button timePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_picker);
        //init time picker button
        timePicker = (Button) findViewById(R.id.timePickerButton);
        timePicker.setOnClickListener(this);
        Log.d(LOG_TAG, "TimePickerActivity onCreate");
    }

    /**
     * show time picker dialog
     *
     * @param v
     */
    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.timePickerButton) {
            showTimePickerDialog(v);
        }
    }
}
