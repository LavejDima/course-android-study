package com.android.study.hello_world;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		//set listener to button for delete
		findViewById(R.id.buttonForDelete).setOnClickListener(buttonForDeleteOnCklickListener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	OnClickListener buttonForDeleteOnCklickListener = new OnClickListener() {

		/**
		 * delete button if listener is set to button for delete
		 */
		@Override
		public void onClick(View v) {
			if (v.getId() == R.id.buttonForDelete) {
				deleteButton((Button) v);
			}
		}
	};

	/**
	 * remove button from view group
	 * 
	 * @param button
	 */
	private void deleteButton(Button button) {
		((ViewGroup) button.getParent()).removeView(button);
	}
}
